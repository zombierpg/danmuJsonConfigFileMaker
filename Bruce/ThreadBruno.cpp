#include "stdafx.h"
#include "ThreadBruno.h"
#include "BruceDlg.h"
#include "json/json.h"

CThreadBruno::CThreadBruno()
    :m_pThread_work(NULL)
{
    m_hEvent_Sleep = CreateEvent(NULL, FALSE, FALSE, NULL);
}

CThreadBruno::~CThreadBruno()
{
    CloseHandle(m_hEvent_Sleep);
}
CThreadBruno& CThreadBruno::instance()
{
    static CThreadBruno instance;
    return instance;
}
UINT __cdecl Woker_ThreadProc(LPVOID pParam);
bool CThreadBruno::Start()
{
    LoggerPtr Logger = Logger::getRootLogger();
    //LOG4CXX_DEBUG(Logger, "this is log4cxx test");
    // 首先创建线程
    // TODO: 在此添加额外的初始化代码
    if (m_pThread_work == NULL)
    {
        m_pThread_work = AfxBeginThread(Woker_ThreadProc, this);
    }
    return true;
}

UINT CThreadBruno::Loop(LoggerPtr Logger)
{
    //! 检查目录的所有文件夹
    CString strA;
    CBruceDlg::instance()->m_edtPath.GetWindowText(strA);
    CString basepath = strA;
    CFileFind finder;
    BOOL   bFound;
    m_mapCateData.clear();
    bFound = finder.FindFile(basepath + L"\\*.* ");
    while (bFound)
    {
        bFound = finder.FindNextFile();
        CString   sFilePath = finder.GetFilePath();
        if (finder.IsDirectory())
        {
            if (!finder.IsDots())
            {
                CheckCategory(sFilePath);
            }
        }
    }
    finder.Close();
    if (m_mapCateData.size() > 0)
    {
        // 写入本地文件
        Json::Value root;
        map<CString, vector<CString>>::iterator iter = m_mapCateData.begin();
        for (; iter != m_mapCateData.end(); iter++)
        {
            CString strName = iter->first;
            CW2A pszA(strName);
            string strName_Ascii = pszA;
            vector<CString>& vec_CfgFile = iter->second;
            vector<string> vec_CfgFile_Ascii;
            Json::Value cate;
            for (vector<CString>::iterator iter_vec = vec_CfgFile.begin(); iter_vec != vec_CfgFile.end(); iter_vec++)
            {
                CString strCfgName = *iter_vec;
                CW2A pszA2(strCfgName);
                string strCfgName_Ascii = pszA2;
                vec_CfgFile_Ascii.push_back(strCfgName_Ascii);
                Json::Value  val = (string)pszA2;
                cate.append(val);
                const char* psz = val.asCString();
                //OutputDebugStringA(psz);
            }
            root[strName_Ascii]=cate;
        }
        Json::FastWriter writer;
        std::string strOut = writer.write(root);
        //string strOut = root.toStyledString();
        CString path = basepath + "cfg.json";
        CFile file(path,CFile::modeReadWrite |
                   CFile::shareExclusive);
        char pbufRead[10240];
        memset(pbufRead, 0, sizeof(pbufRead));
        int nReadCount = file.Read(pbufRead, sizeof(pbufRead));
        file.SeekToBegin();
        if (strOut != pbufRead)
        {
            file.Write(strOut.c_str(), strOut.length());
            file.SetLength(strOut.length());
        }
        else
        {
            return 1;
        }
        file.Close();
    }
    return 0;
}

UINT CThreadBruno::ThreadRun()
{
    m_bStopThread = false;
    LoggerPtr Logger = Logger::getRootLogger();
    UINT ret = Loop(Logger);
    while (!m_bStopThread)
    {
        switch (ret)
        {
        case 0:
            // 表示更新成功，睡眠10s
            LOG4CXX_INFO(Logger, "更新成功，睡眠10s");
            WaitForSingleObject(m_hEvent_Sleep, 10000);
            break;
        case 1:
            // 表示没有更新，睡眠5s
            LOG4CXX_INFO(Logger, "没有文件需要个更新，睡眠5s")
            WaitForSingleObject(m_hEvent_Sleep, 5000);
            break;
        case -2:
            // 表示服务器连接失败，10秒后重试
            LOG4CXX_INFO(Logger, "服务器连接失败，10秒后重试");
            WaitForSingleObject(m_hEvent_Sleep, 10000);
            break;
        case -3:
            // 表示无法查找到本地DLL文件，10秒后重试
            LOG4CXX_INFO(Logger, "无法查找到本地DLL文件，10秒后重试");
            WaitForSingleObject(m_hEvent_Sleep, 10000);
            break;
        case -5:
            // 表示无法查找到本地DLL文件，10秒后重试
            LOG4CXX_INFO(Logger, "上传文件失败，10秒后重试");
            WaitForSingleObject(m_hEvent_Sleep, 10000);
            break;
        case -10:
            // 表示服务器遇到致命错误，程序立刻退出
            LOG4CXX_ERROR(Logger, "服务器遇到致命错误，程序立刻退出");
            m_bStopThread = true;
            return -1;
            break;
        default:
            break;
        }
        ret = Loop(Logger);
    }
    return 0;
}

void CThreadBruno::CheckCategory(CString sFilePath)
{
    //throw std::logic_error("The method or operation is not implemented.");
    CString temp = sFilePath.Mid(0, sFilePath.GetLength() - 1);
    int lastpos = temp.ReverseFind('\\');
    CString cate = sFilePath.Mid(lastpos + 1, sFilePath.GetLength() - lastpos - 1);
    //
    std::vector<CString> vecCfgs;
    CFileFind finder;
    BOOL   bFound;
    bFound = finder.FindFile(sFilePath + L"\\*_cfg.js ");
    while (bFound)
    {
        bFound = finder.FindNextFile();
        CString   sFilePath = cate+ L"\\"+ finder.GetFileName();
        vecCfgs.push_back(sFilePath);
    }
    finder.Close();
    //!
    if (lastpos != -1 && vecCfgs.size()!=0)
    {
        //CString temp2 = sFilePath.Mid(0, lastpos);
        //int pos2 = temp2.ReverseFind('\\');
        m_mapCateData.insert(make_pair(cate, vecCfgs));
    }
}

UINT __cdecl Woker_ThreadProc(LPVOID pParam)
{
    CThreadBruno* pWoker = (CThreadBruno*)pParam;
    if (pWoker != NULL)
    {
        UINT uRet = pWoker->ThreadRun();
    }
    // 如果线程退出，发送WM_QUIT消息，退出程序。
    //	pWoker->SendCloseMsg();
    return -1;
}