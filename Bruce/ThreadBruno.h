#pragma once

#include "log4cxx/logger.h"
#include "log4cxx/propertyconfigurator.h"

#include <vector>
#include <map>

using namespace std;
using namespace log4cxx;
using namespace log4cxx::helpers;

class CThreadBruno {
public:
    CThreadBruno();
    ~CThreadBruno();
    //!
    static CThreadBruno& instance();
    //!
    bool Start();
    //!
    UINT Loop(LoggerPtr Logger);
    //!
    UINT ThreadRun();
    void CheckCategory(CString sFilePath);
private:
    //////////////////////////////////////////////////////////////////////////
    CWinThread * m_pThread_work;
    bool		m_bStopThread;
    HANDLE		m_hEvent_Sleep;
    map<CString, vector<CString>>	m_mapCateData;
};

